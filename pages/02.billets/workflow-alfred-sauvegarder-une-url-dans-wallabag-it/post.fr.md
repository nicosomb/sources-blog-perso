---
title: 'Workflow Alfred : sauvegarder une URL dans wallabag.it'
published: true
taxonomy:
    category:
        - Blog
    tag:
        - wallabag
        - alfred
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

Si vous utilisez [Alfred](https://www.alfredapp.com/) (oui, ça suppose que vous êtes sous Mac), voici rapidement comment sauvegarder une URL dans votre compte [wallabag.it](https://wallabag.it/fr). 

[Téléchargez ce workflow](Save%20to%20wallabag.alfredworkflow), installez-le (en double cliquant dessus normalement) et lorsque vous affichez le formulaire d'Alfred, tapez : `w http://URL-À-SAUVEGARDER`. Validez et votre navigateur vous ouvre wallabag et ajoute l'URL dans votre compte. 

Facile. 