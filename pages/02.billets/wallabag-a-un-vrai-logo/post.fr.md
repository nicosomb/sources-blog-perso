---
title: 'wallabag a un vrai logo'
date: '11-02-2014 08:17'
taxonomy:
    category:
        - Blog
    tag:
        - wallabag
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

[Je le disais il y a quelques jours](../un-mal-pour-un-bien), cette histoire ne m'a pas apporté que des soucis.

Encore une preuve : Maylis Agniel, qui travaille chez [wearemd](http://wearemd.com/), vient de créer le logo de wallabag. Et franchement, ça claque !

![logo de wallabag](wallabag-logo.svg)

**Un énorme merci à elle.**