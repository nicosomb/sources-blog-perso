---
title: 'Et Mozilla décida de remettre Pocket en addon'
date: '06-11-2015 11:00'
taxonomy:
    category:
        - Blog
    tag:
        - pocket
        - mozilla
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

**move pocket to a built-in add-on**, c'est le titre du [ticket ouvert sur Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=1215694) le 16 octobre dernier par Shane Caraveo.

> We're moving pocket to a built-in addon.  This will facilitate user choice (I rip the pockets off everything I own), alternate firefox distributions that do not want to include the feature, etc.  As well it will help to identify any potential issues with using add-ons for feature implementation.

Pour rappel :

* [Pourquoi Mozilla se trompe](../pourquoi-mozilla-se-trompe)
* [Mozilla / Pocket : la réponse de Mark Mayo](../reponse-mozilla)
* Comment désactiver Pocket dans Firefox

Alors, forcément, c'est une bonne nouvelle que Firefox n'embarque plus nativement Pocket (pour laisser le choix aux utilisateurs, tout ça), mais ce rapide retour en arrière est quand même inquiétant sur la gestion de tout ça en interne.