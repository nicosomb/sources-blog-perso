---
title: 'PHP Tour à Clermont-Ferrand, les slides'
date: '27-05-2016 12:00'
taxonomy:
    category:
        - Blog
    tag:
        - wallabag
        - phptour
        - php
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

Cette semaine, c'était le PHP Tour à Clermont-Ferrand. On était présent avec [Jérémy](https://twitter.com/j0k) pour parler de wallabag et de la migration d'un projet PHP tout pourri mais qui marche à une application plus qualitative, basée sur Symfony (mais c'est pas uniquement pour ça que c'est plus qualitatif, lisez les slides). 

Pour notre présentation, je trouve qu'on s'en est plutôt bien sorti, malgré le manque de préparation (on n'a pas répété à deux avant de passer, le midi on se demandait encore qui parlait à quel moment, etc.). Mais les retours qu'on a eus pour l'instant semblent corrects.

##### Slides de "wallabag : comment on a migré vers Symfony 3"

* Sur slideshare : [http://fr.slideshare.net/nicosomb/wallabag-comment-on-a-migre-vers-symfony3](http://fr.slideshare.net/nicosomb/wallabag-comment-on-a-migre-vers-symfony3)
* en version web (appuyez sur la touche S de votre clavier pour lire nos notes) : [https://nicosomb.github.io/talk-phptour-2016/](https://nicosomb.github.io/talk-phptour-2016/)

Si vous avez assisté à la conf, n'hésitez pas à laisser [un avis sur joind](https://joind.in/event/php-tour-clermont-ferrand-2016/wallabag-comment-on-a-migr-vers-symfony3).

![Notre conf](phptour01.jpg)

![Wow beaucoup de monde](phptour02.jpg)

wallabag était plutôt bien représenté : hormis notre conf, [Joel Wurtz](https://twitter.com/JoelWurtz) a dockerizé wallabag lors d'un atelier et [Kévin Gomez](https://twitter.com/kphoen) a présenté [RulerZ](https://github.com/K-Phoen/rulerz) avec forcément une slide sur son implémentation dans wallabag. Merci à vous !

![Santé !](phptour05.jpg)

**Merci à l'AFUP et Clermont'ech pour l'organisation de l'événement !**

![Santé !](phptour03.jpg)

![wallabag everywhere](phptour04.jpg)