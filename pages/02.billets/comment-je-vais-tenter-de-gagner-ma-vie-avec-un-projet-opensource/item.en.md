---
title: 'How I will try to earn a living with an open source project'
published: true
taxonomy:
    category:
        - Blog
    tag:
        - wallabag
---

This post is the continuation of ["Comment je vais tenter de gagner un peu d'argent avec un projet opensource"](https://nicolas.loeuillet.org/billets/service-wallabag-it) (written in 🇫🇷). 

---

Some explanations for my english readers: I started [wallabag](https://github.com/wallabag/wallabag) four years ago and I launched a SaaS to use this application easily, without installing it.  
[You can read this blog post in english on wallabag.it to have more news](https://blog.wallabag.it/2016/12/20/time-take-stock/). 

---

On december, I launched [wallabag.it](https://wallabag.it/en). I know that it’s not a huge success, but without any advertising (except on twitter, with my geek followers), I think it’s a good start. It’s also because I don’t need to earn much money, I’ve got an other jobs to pay bills.

## Some figures

In few days, it will be three months since the service was launched.   
Here a some figures:
* about 250 customers
* more than 750,000 saved web articles
* many many positive feedbacks
* some little bugs, due to the « success » of the project (I didn’t expect you, crazy guys, to many 30,000 articles). 

So all is fine … if you don’t want to earn a living with this project. 

## Next?

The more you have, the more you want. Now, I hope this project get bigger and bigger. **I want to earn a living with wallabag**. 

But I need more time.  
So I need to leave my current job as a web developer in a french web agency. 

I’m analyzing all the possibilities to work exclusively for wallabag. 

## Marketing & co

In coming days, I will work on the marketing side of the project. I’m not a marketing expert but it will be necessary to improve this part.  
I’ve got two books to read, tables to fill, a Lean Canvas to edit, etc. **Happyness**. 

![](livres.jpg)

I will give you more news here or on [twitter](https://twitter.com/nicosomb). 