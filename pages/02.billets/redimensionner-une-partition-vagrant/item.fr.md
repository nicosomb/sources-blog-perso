---
title: 'Redimensionner une partition Vagrant'
published: false
date: '28-12-2017 14:46'
taxonomy:
    category:
        - Blog
    tag:
        - vagrant
        - macos
intro:
    enabled: false
    fullscreen: false
    text_enabled: false
link: 'https://tvi.al/resize-sda1-disk-of-your-vagrant-virtualbox-vm/'
---

Bien pratique pour augmenter la taille de sa partition Vagrant très facilement.