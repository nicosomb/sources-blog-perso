---
title: 'Plus de pélican, c''est grav'
published: true
taxonomy:
    category:
        - Blog
    tag:
        - php
        - blog
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

Voila, on termine l'année avec un titre de billet bien pourri. 

Depuis un petit moment déjà, ce blog était généré via [le superbe outil Pelican](http://blog.getpelican.com/). Pratique, c'est du statique, tout était hébergé sur GitHub : un commit, un push et le site se mettait à jour tout seul. 

N'étant pas expert Python (et pas trop le temps pour apprendre, c'est pas l'envie qui manque pourtant), je voulais revenir dans un peu plus de confort, au cas où, pour bidouiller un peu mon blog. Du coup, voici un nouveau blog, généré avec [Grav](https://getgrav.org/), qui me faisait de l'œil depuis quelques temps déjà. 

C'est aussi du statique (fichiers markdown), mais c'est plus simple à gérer : y'a un plugin d'administration qui permet de publier les billets, d'installer de nouveaux plugins, etc. 

J'en ai profité pour virer Disqus (dommage pour les commentaires publiés là-bas, ça vous apprendra à poster sur un service qui ne respecte peut-être pas votre vie privée après tout !) parce qu'il y a un plugin de commentaires.

Ce n'est plus hébergé chez GitHub 🎉 mais chez Web4all.

Puisque je ne crois pas qu'il soit possible d'avoir un flux RSS par tag, je ne peux plus publier sur le Planet Libre. Tant pis. 

Et j'en ai profité pour rebasculer tout ça sur l'URL http://nicolas.loeuillet.org. Et j'ai fait un peu de nettoyage, certains articles qui étaient périmé ne sont plus là. Les redirections qui vont bien devraient être là pour les billets logiquement. 

Bonne fin d'année !