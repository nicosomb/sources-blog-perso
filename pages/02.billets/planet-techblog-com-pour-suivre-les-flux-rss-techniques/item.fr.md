---
title: 'Planet-Techblog.com pour suivre les flux RSS techniques'
date: '05-01-2017 17:24'
taxonomy:
    category:
        - Blog
    tag:
        - planet-tech
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

Il y a quelques semaines, [Éric D. a tweeté ça](https://twitter.com/edasfr/status/794111970102353920) :

> On parle de blog d’équipes tech, mon équipe me demande des exemples. J’ai les miens mais vous m’aider à élargir avec d’autres trucs biens ?
> On me dit d’en faire un pad quelque part, super idée donc…

J'ai participé au pad en ajoutant 2/3 liens. 

Et puis de tweet en aiguille, en discutant avec Éric et mereteresa, [l'idée d'un OPML puis d'un planet arrive](https://twitter.com/mereteresa/status/794124394419384320). 

Je génère donc un OPML que j'avais partagé et faute de trouver un outil facile à mettre en place pour créer un planet, j'avais mis de côté l'idée. 

Aujourd'hui, toujours sur twitter, [Loïc me parle](https://twitter.com/loicmathaud/status/816932017027907584) de [moonmoon](http://moonmoon.org/). Super, un outil facile à mettre en place, en PHP et qui marche (juste un souci avec la création du répertoire de cache, je regarderai ça plus tard pour corriger sur le dépôt). 

Bref, tout ça pour vous annoncer l'ouverture de [Planet Tech](http://planet-techblog.com/).  [Y'a un flux Atom](http://planet-techblog.com/atom.php), [y'a un OPML](http://planet-techblog.com/custom/people.opml) et [une jolie page d'archives](http://planet-techblog.com/?type=archive).

Si vous avez des blogs techniques à ajouter, [envoyez-moi un mail](mailto:nicolas@loeuillet.org). 

**RSS is not dead!**