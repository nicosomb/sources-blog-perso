---
title: 'Y''a quand même des gens sympas hein'
date: '14-02-2014 07:52'
taxonomy:
    category:
        - Blog
    tag:
        - wallabag
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

Cette semaine, j'étais [un peu grognon](../on-ma-prevenu).

Et puis, y'a des gens sympas.

* Merci à Maylis pour le logo.
* Merci Thomas pour le nouveau design.
* Merci à mariroz pour les nouvelles traductions (Polonais et Ukrainien).
* Merci pour les tweets, les emails, les inscriptions sur Framabag qui augmentent très bien, etc.

Tout ça la même semaine <3

Ça fait un peu Cérémonie des Césars, et alors ? :-) Ça fait du bien aussi des fois.