---
title: 'poche 1.0.0'
date: '04-10-2013 09:56'
taxonomy:
    category:
        - Blog
    tag:
        - wallabag
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
---

Six mois. [Entre le premier commit](../poche-pour-remplacer-instapaper-pocket-et-readability) (le 3 avril 2013) et la 1.0.0 (3 octobre 2013), six mois tout juste.

**Et voila, la 1.0.0 de poche est maintenant disponible.**

[Un nouveau site est disponible](http://wallabag.org/). Plus complet, plus pratique, homogène.

Pour télécharger poche et les applications tierces (pour Firefox, Chrome, Windows Phone & Android), [c'est par là que ça se passe](http://www.wallabag.org).

Merci beaucoup à chacun d'entre vous : [contributeurs](https://github.com/wallabag/wallabag/graphs/contributors), [dénicheurs de bugs](https://github.com/wallabag/wallabag/issues), les utilisateurs de poche et de [poche hosting](https://www.framabag.org/), les [followers](http://twitter.com/wallabagapp), etc.

Et un **énorme** merci à ma femme. Sans elle, ce projet ne serait pas là où il est aujourd'hui.

Amusez-vous bien avec poche.