---
title: 'Curriculum Vitæ'
content:
    items: '@self.modular'
    order:
        by: date
        dir: desc
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
header_image: 'https://nicolas.loeuillet.org/user/pages/01.billets/4e511fe4304cba39484d22a31c66c9f4dfab2eb9-home-bg.jpeg'
---

## Nicolas Lœuillet - Expert technique PHP - 11 ans d'expérience

[CV au format PDF](cv.nicolas.loeuillet.pdf) 
