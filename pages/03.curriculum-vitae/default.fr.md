---
title: 'Curriculum Vitæ'
external_links:
    process: true
    title: false
    no_follow: true
    target: _self
    mode: active
header_image: 'https://nicolas.loeuillet.org/user/pages/01.billets/4e511fe4304cba39484d22a31c66c9f4dfab2eb9-home-bg.jpeg'
---

Mon [CV est disponible au format PDF](cv.nicolas.loeuillet.pdf) (ou sur [Linkedin](https://www.linkedin.com/in/nicolas-l%C5%93uillet/)).

Je donne également des conférences concernant wallabag, la vie privée, le PHP ou la vie numérique. 

#### Mon après-moi numérique

Lors d'un [Apéro Web](http://www.aperoweb.fr/post/2017/10/30/Ap%C3%A9ro-Web-Calais-mercredi-8-novembre), mercredi 8 novembre 2017 - Calais.

> Comment préparer tout ça dès maintenant (parce qu’après il sera trop tard).
> 
> Aujourd’hui, nous allons parler d’un sujet très réjouissant : la mort. Enfin plutôt : que deviendront nos traces numériques une fois que nous ne serons plus là ? On peut également envisager que nous allons perdre la mémoire au cours de notre vie et donc réfléchir à tout ça aujourd’hui nous aidera dans quelques années.

###### slides

[Elles sont là](https://www.slideshare.net/nicosomb/mon-aprsmoi-numrique-dsfdsf)

#### wallabag, comment on a migré vers Symfony3

Lors du [PHP Tour](http://event.afup.org/php-tour-2016/programme/#1777), lundi 23 mai 2016 - Clermont-Ferrand. Avec Jérémy Benoist.

> wallabag est une application opensource de lecture différée : elle vous permet de mettre de côté la version épurée d'un article pour la consulter plus tard où que vous soyez. Créée il y a 3 ans à base de fichiers PHP comme on faisait en 2005, nous avons décidé il y a maintenant un peu plus d'un an de migrer le projet à Symfony. Au cours de ce talk, nous présenterons donc le projet wallabag et tout son écosystème : son concept, son socle technique (API REST, tests unitaires, Rulerz, RabbitMQ, Capistrano), les difficultés rencontrées, la communauté et les projets qui tournent autour, la roadmap pour les semaines à venir.

###### slides

[Elles sont là](http://fr.slideshare.net/nicosomb/wallabag-comment-on-a-migre-vers-symfony3)

#### Framabag, wallabag, together let's decentralize internet !

Lors de [fOSSa](https://fossa.inria.fr/session/owncloud-project/), vendredi 25 septembre 2015 - Nantes.

> Dégooglisons Internet (Ungooglize Internet) is a project by Framasoft, a non-profit promoting free and open source software and culture. Degooglisons Internet has 3 goals:
>
> * Rise the level of awareness among mainstream Internet users about the risks of excessive centralization of the web by corporate giants such as Google, Apple, Facebook, Amazon and Microsoft.
> * Provide a proof of concept that free and open source software is a sustainable solution to the problem. Framasoft launched a campaign to provide and promote free and libre alternatives, respecting data and user privacy. For every service mass-collecting user data (eg. Facebook,Google Docs, Dropbox, Skype, etc.), Framasoft is offering free/libre, ethic and decentralized alternatives.
> * Spread knowledge, practice and services with tutorials and federate alternate independent hosting services. The more free/libre services will be provided, the more decentralized the internet will remain.

###### slides

[Elles sont là](http://fr.slideshare.net/nicosomb/framabag-wallabag-together-lets-decentralize-internet)

###### vidéo

[C'est par ici !](http://videos.rennes.inria.fr/seminaire-FOSSA2015/expose-NicolasLoeuilletFossa2015.mp4)

#### Effectuez votre veille en toute liberté !

Lors des [rencontres Mondiales du Logiciel Libre](https://2015.rmll.info/effectuez-votre-veille-en-toute-liberte?lang=fr), mercredi 8 juillet 2015 - Beauvais.

> Jamais assez de temps pour faire votre veille au quotidien, du coup vous conservez votre navigateur préféré ouvert avec 42 onglets ou alors vous stockez tout chez Pocket / Instapaper ou encore Readability ?
>
> Aujourd’hui, faites ça simplement et surtout librement avec wallabag, une application de lecture différée libre et opensource.
Et découvrez Framabag, la version SaaS de wallabag, par l’équipe de Framasoft !

###### slides

[Elles sont là](http://fr.slideshare.net/nicosomb/effectuez-votre-veille-en-toute-liberte-rmll-2015-beauvais)

###### vidéo

[C'est par ici !](https://rmll.ubicast.tv/permalink/v1253b457494df1f15k0/iframe) (pensez à désactiver votre bloqueur de publicité pour qu'elle fonctionne)