---
title: 'À propos'
intro:
    enabled: false
    fullscreen: false
    text_enabled: false
---

Bonjour, je suis Nicolas Lœuillet. 

Je suis lead développeur chez [Code Rhapsodie](https://www.code-rhapsodie.fr/), cabinet d'expertise qui intervient en développement (eZ Publish, Symfony), conseils AMOA et AMOE, audits, infra et formations. 

J'ai également créé [wallabag](https://wallabag.org/fr), une application de lecture différée open source (écrite en PHP). J'ai lancé le SaaS [wallabag.it](https://wallabag.it/fr), qui permet – pour un montant dérisoire – d'utiliser l'application facilement. 

Depuis mars 2018, je suis également maire de la commune de Rinxent. [J'en parle ici](https://nicolas.loeuillet.org/billets/pourquoi-et-comment-je-suis-devenu-le-maire-de-ma-commune). 

Sur le côté personnel, je suis mari, papa de deux enfants et je joue au basket.

~~J'interviens également en école primaire pour former les élèves de Rinxent aux outils numériques. Pour aller plus loin avec eux, nous avons créé l'[association Rinx'Net](http://rinx-net.fr/) pour apprendre le développement informatique (avec Scratch).~~

Vous trouverez [mon CV ici](https://nicolas.loeuillet.org/curriculum-vitae) ou alors sur [Linkedin](https://www.linkedin.com/in/nicolas-l%C5%93uillet). Vous me trouverez aussi sur [GitHub](https://github.com/nicosomb) et [twitter](https://twitter.com/nicosomb).

Ce site utilise Grav, un [générateur de site statique](https://getgrav.org/). 